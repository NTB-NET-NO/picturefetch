Imports System.Configuration
Imports System.xml
Imports System.io
Imports ntb_FuncLib.LogFile

Public Class DebugForm
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        components = New System.ComponentModel.Container
        Me.Text = "DebugForm"
    End Sub

#End Region

    Dim logFolder As String
    Dim JobList As Hashtable = New Hashtable

    Private Sub DebugForm_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Initiation
        ' logFolder = AppSettings("logFolder")
        logFolder = ConfigurationManager.AppSettings("logFolder")
        Directory.CreateDirectory(logFolder)

        WriteLog(logFolder, "Starting PictureFetchService...")

        Dim doc As XmlDocument = New XmlDocument
        Dim node As XmlNode
        Dim f As FetchJob = New FetchJob

        doc.Load(ConfigurationManager.AppSettings("configFile"))

        For Each node In doc.SelectNodes("pictureJobs/fetchJob")

            Dim job As FetchJob = New FetchJob
            job.Init(node)
            JobList.Add(node.Attributes("XMLInputPath").Value, job)

        Next

        WriteLog(logFolder, "Picture Fetch Service started.")
        WriteLogNoDate(logFolder, "---------------------------------------------------------------------------------------------")

    End Sub
End Class
