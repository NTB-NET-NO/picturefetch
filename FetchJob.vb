Imports System.Configuration
Imports System.IO
Imports System.Text
Imports System.Drawing
Imports ntb_FuncLib.LogFile
Imports System.Net
Imports System.Xml

Public Class FetchJob
    Inherits System.ComponentModel.Component

#Region " Component Designer generated code "

    Public Sub New(ByVal Container As System.ComponentModel.IContainer)
        MyClass.New()

        'Required for Windows.Forms Class Composition Designer support
        Container.Add(Me)
    End Sub

    Public Sub New()
        MyBase.New()

        'This call is required by the Component Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Component overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.
    'Do not modify it using the code editor.
    Friend WithEvents FileWatcher As System.IO.FileSystemWatcher
    Friend WithEvents PollTimer As System.Timers.Timer
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.FileWatcher = New System.IO.FileSystemWatcher
        Me.PollTimer = New System.Timers.Timer
        CType(Me.FileWatcher, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PollTimer, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'FileWatcher
        '
        Me.FileWatcher.EnableRaisingEvents = True
        Me.FileWatcher.NotifyFilter = CType((System.IO.NotifyFilters.FileName Or System.IO.NotifyFilters.LastWrite), System.IO.NotifyFilters)
        '
        'PollTimer
        '
        Me.PollTimer.Interval = 5000
        CType(Me.FileWatcher, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PollTimer, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region

    'Logfile mutex control
    Private Shared _logMute As Threading.Mutex = New Threading.Mutex(False)

    'File extensions
    Private Shared extensions As Hashtable = New Hashtable

    'Fetch log
    Private fetched As Hashtable = New Hashtable

    'Retry log
    Private retries As Hashtable = New Hashtable

    'Vars
    Private logFolder As String

    Private inputFolder As String
    Private outputFolder As String
    Private doneFolder As String
    Private errorFolder As String

    Private xmlMetaURL As String()
    Private xmlBASEXpath As String
    Private xmlRIDXpath As String

    'Access controll
    Private username As String
    Private password As String

    Private config As XmlNode

    Public Sub Init(ByVal confNode As XmlNode)

        'File extensions
        extensions.Item("image/jpeg") = ".jpg"
        extensions.Item("image/gif") = ".gif"
        extensions.Item("application/pdf") = ".pdf"

        'Initiation
        logFolder = ConfigurationManager.AppSettings("logFolder")

        config = confNode

        inputFolder = config.Attributes("XMLInputPath").Value
        outputFolder = config.Attributes("PicturePath").Value
        doneFolder = config.Attributes("DonePath").Value
        errorFolder = config.Attributes("ErrorPath").Value

        Dim tmp As String
        Try
            xmlMetaURL = config.Attributes("useXMLMetaURLs").Value.Split({"|"c}, StringSplitOptions.RemoveEmptyEntries)
            xmlBASEXpath = config.Attributes("xmlBASEXpath").Value
            xmlRIDXpath = config.Attributes("xmlRIDXpath").Value
            tmp = " - Using XMLMETA at" & config.Attributes("useXMLMetaURLs").Value
        Catch ex As Exception
        End Try

        Directory.CreateDirectory(inputFolder)
        Directory.CreateDirectory(outputFolder)
        Directory.CreateDirectory(doneFolder)
        Directory.CreateDirectory(errorFolder)

        Try
            username = config.Attributes("username").Value
            password = config.Attributes("password").Value
        Catch ex As Exception
        End Try

        FileWatcher.Path = inputFolder
        FileWatcher.Filter = "*.xml"
        PollTimer.Start()

        _logMute.WaitOne()
        WriteLog(logFolder, "Picturefetch object initiated for inputfolder: '" & inputFolder & "'" & tmp)
        _logMute.ReleaseMutex()
    End Sub

    Private Sub FileWatcher_Created(ByVal sender As Object, ByVal e As System.IO.FileSystemEventArgs) Handles FileWatcher.Created
        PollTimer.Stop()
        FileWatcher.EnableRaisingEvents = False

        Try
            DoAllFiles()
        Catch ex As Exception
            WriteErr(logFolder, "DoAllFiles() error encountered: " & ex.Message, ex)
        End Try

        FileWatcher.EnableRaisingEvents = True
        PollTimer.Interval = ConfigurationManager.AppSettings("pollInterval") * 1000
        PollTimer.Start()
    End Sub

    Private Sub PollTimer_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles PollTimer.Elapsed
        PollTimer.Stop()
        FileWatcher.EnableRaisingEvents = False

        Try
            DoAllFiles()
        Catch ex As Exception
            WriteErr(logFolder, "DoAllFiles() error encountered: " & ex.Message, ex)
        End Try

        FileWatcher.EnableRaisingEvents = True
        PollTimer.Interval = ConfigurationManager.AppSettings("pollInterval") * 1000
        PollTimer.Start()
    End Sub

    Private Sub DoAllFiles()

        Dim files As String() = Directory.GetFiles(inputFolder)

        Threading.Thread.Sleep(3000)

        Dim xmldoc As XmlDocument = New XmlDocument

        Dim mediaList As XmlNodeList = Nothing
        Dim mediaNode As XmlNode = Nothing

        Dim jobList As XmlNodeList = config.SelectNodes("url")
        Dim jobNode As XmlNode = Nothing

        Dim delay As Integer = 0
        Dim prefix As String = ""
        Dim nameXpath As String = ""
        Dim tp As String = ""
        Dim id As String = ""
        Dim ext As String = ""

        Dim f As String = ""
        For Each f In files

            'Get the media reference list
            Try
                _logMute.WaitOne()
                WriteLog(logFolder, "New XML file: '" & f & "'")
                xmldoc.Load(f)
                mediaList = xmldoc.SelectNodes("/nitf/body/body.content/media/media-reference")
            Catch ex As Exception
                WriteErr(logFolder, "Error in input XML '" & f & "'", ex)
                File.Copy(f, errorFolder & "\" & Path.GetFileName(f), True)
                File.Delete(f)
            Finally
                _logMute.ReleaseMutex()
            End Try

            'Get the pictures
            Try
                _logMute.WaitOne()
                For Each mediaNode In mediaList
                    prefix = String.Empty
                    nameXpath = String.Empty
                    delay = 0
                    id = mediaNode.Attributes("source").Value.Trim
                    ext = extensions(mediaNode.Attributes("mime-type").Value)

                    'Handle fetch based on XMLMeta 
                    If Not xmlMetaURL Is Nothing Then

                        'Handle XMLMeta, try all defined in order, first hit accepted
                        Dim xmlMeta As XmlDocument = Nothing
                        For Each url As String In xmlMetaURL
                            xmlMeta = GetXMLMeta(url.Replace("{refptr}", id))
                            If Not xmlMeta Is Nothing And xmlMeta.DocumentElement.Name() <> "errorMessage" Then Exit For
                        Next

                        'Then do error checking
                        If xmlMeta Is Nothing Then
                            Throw New Exception("XMLMeta error: Content is empty")
                        End If
                        If xmlMeta.DocumentElement.Name() = "errorMessage" Then
                            Throw New Exception(String.Format("XMLMeta error {0}: {1}", xmlMeta.SelectSingleNode("/errorMessage/@code").Value _
                                                              , xmlMeta.SelectSingleNode("/errorMessage").InnerText))
                        End If

                        For Each jobNode In jobList

                            'Get type of URL
                            Try
                                tp = jobNode.Attributes("type").Value.ToUpper()
                            Catch ex As Exception
                                tp = "IMAGE-XML"
                            End Try

                            If (tp = "IMAGE-XML" And mediaNode.Attributes("mime-type").Value.StartsWith("image")) Or
                                (tp = "PDF-XML" And mediaNode.Attributes("mime-type").Value.EndsWith("pdf")) Then

                                'Get properties
                                Try
                                    delay = CInt(jobNode.Attributes("delay").Value)
                                Catch ex As Exception
                                    delay = 0
                                End Try
                                Try
                                    prefix = jobNode.Attributes("prefix").Value
                                Catch ex As Exception
                                    prefix = ""
                                End Try
                                Try
                                    nameXpath = jobNode.Attributes("nameXpath").Value
                                Catch ex As Exception
                                    nameXpath = Nothing
                                End Try

                                'Build URL
                                Dim url As String = jobNode.InnerText()
                                If url.StartsWith("XPATH#") Then
                                    url = xmlMeta.SelectSingleNode(url.Substring(6)).InnerText
                                Else
                                    url = url.Replace("{base}", xmlMeta.SelectSingleNode(xmlBASEXpath).InnerText())
                                    url = url.Replace("{rid}", xmlMeta.SelectSingleNode(xmlRIDXpath).InnerText())
                                End If

                                'Get filename
                                If Not nameXpath Is Nothing Then
                                    Try
                                        id = xmlMeta.SelectSingleNode(nameXpath).InnerText()
                                    Catch ex As Exception
                                    End Try
                                End If

                                'Stop on max retries 
                                If (retries.ContainsKey(prefix & id) And _
                                        retries(prefix & id) > 10) Then
                                End If

                                If Not fetched.ContainsKey(prefix & id) Then
                                    'Fix to prevent server chocking
                                    If delay > 0 Then
                                        WriteLog(logFolder, "Delaying download " & delay & " seconds to prevent server from chocking.")
                                        System.Threading.Thread.Sleep(delay * 1000)
                                    End If

                                    SaveXMLItem(id, prefix, url)
                                    WriteLog(logFolder, "Item downloaded: '" & prefix & id & "'")
                                Else
                                    WriteLog(logFolder, "Item already fetched: '" & prefix & id & "'")
                                End If

                            End If
                        Next


                        'Handle images
                    ElseIf mediaNode.Attributes("mime-type").Value.StartsWith("image") And id <> "" Then

                        For Each jobNode In jobList

                            'Get type of URL
                            Try
                                tp = jobNode.Attributes("type").Value.ToUpper()
                            Catch ex As Exception
                                tp = "IMAGE"
                            End Try

                            If tp = "IMAGE" Then
                                'Get properties
                                Try
                                    prefix = jobNode.Attributes("prefix").Value
                                    delay = CInt(jobNode.Attributes("delay").Value)
                                Catch ex As Exception
                                    delay = 0
                                End Try

                                'Stop on max retries 
                                If (retries.ContainsKey(prefix & id & ext) And _
                                        retries(prefix & id & ext) > 10) Then

                                End If


                                If Not fetched.ContainsKey(prefix & id & ext) Then

                                    'Fix to prevent server chocking
                                    If delay > 0 Then
                                        WriteLog(logFolder, "Delaying download " & delay & " seconds to prevent server from chocking.")
                                        System.Threading.Thread.Sleep(delay * 1000)
                                    End If

                                    GetPicture(id & ext, prefix, jobNode.InnerText())
                                    WriteLog(logFolder, "Picture downloaded: '" & prefix & id & ext & "'")
                                Else
                                    WriteLog(logFolder, "Picture already fetched: '" & prefix & id & ext & "'")
                                End If
                            End If

                        Next

                        'Handle PDFs
                    ElseIf mediaNode.Attributes("mime-type").Value.EndsWith("pdf") And id <> "" Then
                        For Each jobNode In jobList

                            'Get type of URL
                            Try
                                tp = jobNode.Attributes("type").Value.ToUpper()
                            Catch ex As Exception
                                tp = "IMAGE"
                            End Try

                            If tp = "PDF" Then
                                'Get properties
                                Try
                                    ext = ""
                                    prefix = jobNode.Attributes("prefix").Value
                                    delay = CInt(jobNode.Attributes("delay").Value)
                                Catch ex As Exception
                                    delay = 0
                                End Try

                                If Not fetched.ContainsKey(prefix & id & ext) Then

                                    'Fix to prevent server chocking
                                    If delay > 0 Then
                                        WriteLog(logFolder, "Delaying download " & delay & " seconds to prevent server from chocking.")
                                        System.Threading.Thread.Sleep(delay * 1000)
                                    End If

                                    SaveAttachment(id & ext, prefix, jobNode.InnerText())
                                    WriteLog(logFolder, "Attachment downloaded: '" & prefix & id & ext & "'")
                                Else
                                    WriteLog(logFolder, "Attachment already fetched: '" & prefix & id & ext & "'")
                                End If
                            End If
                        Next

                    Else
                        WriteLog(logFolder, "Missing/bad media source ID: '" & id & "' / '" & mediaNode.Attributes("mime-type").Value & "'")
                    End If
                Next

                'Move the file to done
                File.Copy(f, ntb_FuncLib.FuncLib.MakeSubDirDate(doneFolder & "\" & Path.GetFileName(f), File.GetLastWriteTime(f)), True)
                File.Delete(f)

            Catch ex As Exception
                WriteErr(logFolder, "Failed to download media '" & prefix & id & ext & "' from file: " & f, ex)

                If File.GetCreationTime(f).AddMinutes(ConfigurationManager.AppSettings("retryInterval")) < Now Then
                    File.Copy(f, errorFolder & "\" & Path.GetFileName(f), True)
                    File.Delete(f)
                End If
            Finally
                _logMute.ReleaseMutex()
            End Try
        Next
    End Sub

    Private Sub GetPicture(ByVal name As String, ByVal prefix As String, ByVal url As String)

        'Create the HTTP request
        Dim request As HttpWebRequest = WebRequest.Create(url & Path.GetFileNameWithoutExtension(name))

        If username <> "" Then
            request.Credentials = New NetworkCredential(username, password)
        End If

        request.Method = "GET"
        request.Timeout = 10000

        'Get the HTTP response with the resulting data
        Dim response As HttpWebResponse
        response = request.GetResponse()

        Dim img As Image = Image.FromStream(response.GetResponseStream())
        img.Save(outputFolder & "\" & prefix & name)
        response.Close()

        'Cleanup old stuff
        Dim remove As ArrayList = New ArrayList
        Dim key As String
        For Each key In fetched.Keys()
            If fetched(key).addhours(2) < Now Then remove.Add(key)
        Next
        For Each key In remove
            fetched.Remove(key)
        Next

        'Log the fetch as OK
        fetched(prefix & name) = Now

    End Sub

    Private Sub SaveAttachment(ByVal name As String, ByVal prefix As String, ByVal url As String)

        'Create the HTTP request
        Dim request As HttpWebRequest = WebRequest.Create(url & name)

        If username <> "" Then
            request.Credentials = New NetworkCredential(username, password)
        End If

        request.Method = "GET"
        request.Timeout = 10000

        'Get the HTTP response with the resulting data
        Dim response As HttpWebResponse
        response = request.GetResponse()

        'Save data
        Dim out As FileStream = File.Create(Path.Combine(outputFolder, prefix & name))

        Dim ByteBuffer As Byte() = New Byte(511) {}
        Dim ms As MemoryStream = New MemoryStream(ByteBuffer, True)
        Dim rs As Stream = response.GetResponseStream()
        'Dim bytes() As Byte = New Byte(1) {}

        Dim c As Integer
        c = rs.Read(ByteBuffer, 0, ByteBuffer.Length)
        While c > 0
            out.Write(ms.ToArray(), 0, c)
            out.Flush()
            c = rs.Read(ByteBuffer, 0, ByteBuffer.Length)
        End While

        out.Close()
        response.Close()

        'Cleanup old stuff
        Dim remove As ArrayList = New ArrayList
        Dim key As String
        For Each key In fetched.Keys()
            If fetched(key).addhours(2) < Now Then remove.Add(key)
        Next
        For Each key In remove
            fetched.Remove(key)
        Next

        'Log the fetch as OK
        fetched(prefix & name) = Now
    End Sub

    Private Function GetXMLMeta(ByVal url As String) As XmlDocument

        'Create the HTTP request
        Dim request As HttpWebRequest = WebRequest.Create(url)

        If username <> "" Then
            request.Credentials = New NetworkCredential(username, password)
        End If

        request.Method = "GET"
        request.Timeout = 10000

        Try

            'Get the HTTP response with the resulting data
            Dim response As HttpWebResponse
            response = request.GetResponse()

            'Assign XML doc
            Dim doc As New XmlDocument
            doc.Load(response.GetResponseStream)

            WriteLog(logFolder, "XMLMeta downloaded: '" & url & "'")
            Return doc

        Catch ex As Exception
            WriteErr(logFolder, "Failed to download XMLMeta '" & url & "' from file: ", ex)
            Return Nothing
        End Try
    End Function

    Private Sub SaveXMLItem(ByVal name As String, ByVal prefix As String, ByVal url As String)

        'Create the HTTP request
        Dim request As HttpWebRequest = WebRequest.Create(url)

        If username <> "" Then
            request.Credentials = New NetworkCredential(username, password)
        End If

        request.Method = "GET"
        request.Timeout = 10000

        'Get the HTTP response with the resulting data
        Dim response As HttpWebResponse
        response = request.GetResponse()

        Dim ext As String
        ext = extensions(response.Headers("content-type"))

        If Path.GetExtension(name) = String.Empty Then
            name = name + ext
        End If

        'Save data
        Dim out As FileStream = File.Create(Path.Combine(outputFolder, prefix & name))

        Dim ByteBuffer As Byte() = New Byte(511) {}
        Dim ms As MemoryStream = New MemoryStream(ByteBuffer, True)
        Dim rs As Stream = response.GetResponseStream()
        'Dim bytes() As Byte = New Byte(1) {}

        Dim c As Integer
        c = rs.Read(ByteBuffer, 0, ByteBuffer.Length)
        While c > 0
            out.Write(ms.ToArray(), 0, c)
            out.Flush()
            c = rs.Read(ByteBuffer, 0, ByteBuffer.Length)
        End While

        out.Close()
        response.Close()

        'Cleanup old stuff
        Dim remove As ArrayList = New ArrayList
        Dim key As String
        For Each key In fetched.Keys()
            If fetched(key).addhours(2) < Now Then remove.Add(key)
        Next
        For Each key In remove
            fetched.Remove(key)
        Next

        'Log the fetch as OK
        fetched(prefix & name) = Now
    End Sub

End Class

